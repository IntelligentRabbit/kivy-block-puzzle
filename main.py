from threading import Thread

from kivymd.app import MDApp
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen , FadeTransition
from kivy.properties import (
    NumericProperty,
    BooleanProperty,
    StringProperty,
    ObjectProperty,
    ListProperty,
)
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.core.audio import SoundLoader
from kivy.core.text import LabelBase
from kivy.graphics import Line, Color, Rectangle
from kivy.metrics import dp

from io_data import save, load
from dialog import MyMDDialog
from engine import Engine, COLORS
from welcome_screen import WelcomeScreen
from options_screen import OptionsScreen
from loading_screen import LoadingScreen

VERSION = "v. 1.0.1"
W, H = 10, 20
SCREEN_DIVIDER = 18
FPS = 2

Builder.load_file("dialog.kv")
Builder.load_file("welcome_screen.kv")
Builder.load_file("options_screen.kv")
Builder.load_file("loading_screen.kv")


class Game(ScreenManager):
    music_on = BooleanProperty(True)
    sound_on = BooleanProperty(True)
    music_loaded = BooleanProperty(False)
    background = StringProperty("images/backgrounds/bg-1.jpeg")
    backgrounds = ListProperty()
    highscore = NumericProperty(0)
    score = NumericProperty(0)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        for i in range(12):
            bg = f"images/backgrounds/bg-{i+1}.jpeg"
            self.backgrounds.append(bg)

        self.sound_click = SoundLoader.load("sounds/btn_click.wav")
        self.track_1 = SoundLoader.load("sounds/track-1.ogg")
        self.sound_list = [
            self.sound_click,
        ]
        self.music_list = [
            self.track_1,
        ]
        self.music = self.track_1
        self.sound_mute_unmute()
        Clock.schedule_interval(self.play_next_track, .2)
        
        saves_settings = load("saves_settings")
        if len(saves_settings) != 0:
            self.music_on = saves_settings["music_on"]
            self.sound_on = saves_settings["sound_on"]
            self.background = saves_settings["background"]

        saves_highscore = load("saves_highscore")
        if len(saves_highscore) != 0:
            self.highscore = saves_highscore["highscore"]

    def sound_mute_unmute(self):
        if self.sound_on:
            for sound in self.sound_list:
                sound.volume = 1
        else:
            for sound in self.sound_list:
                sound.volume = 0

    def music_mute_unmute(self):
        if self.music_on:
            self.music.volume = .5
        else:
            self.music.volume = 0
                        
    def load_track(self, source):
        track = SoundLoader.load(source)
        if self.music_on:
            track.volume = .5
        else:
            track.volume = 0
        self.music_list.append(track)

    def load_music(self, dt):
        threads = []
        for i in range(3):
            t = Thread(target=self.load_track, args=(f"sounds/track-{i+2}.ogg", ))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()
               
    def play_next_track(self, dt):
        if self.music.state == "stop" and self.music_loaded:
            current_track_number = self.music_list.index(self.music)
            track_number = (current_track_number + 1) % len(self.music_list)
            self.music = self.music_list[track_number]
            self.music.play()
            
class GameScreen(Screen):
    game_widget = ObjectProperty(None)

    def on_pre_enter(self):
        self.game_widget.start()
          

class GameWidget(Widget):
    size_x = NumericProperty(0)
    size_y = NumericProperty(0)
    tile = NumericProperty(0)
    pause = BooleanProperty(False)
    gameover_dialog = None
    pause_dialog = None
    events = []

    def __init__(self, **kwargs):
        super(GameWidget, self).__init__(**kwargs)
        self.size_x, self.size_y = Window.size
        self.tile = self.size_x / SCREEN_DIVIDER
        self.engine = Engine(H, W)
        self.engine.tile = self.tile

    def start(self):
        if self.engine.state == "restart":
            self.engine.__init__(H, W)
        event = Clock.schedule_interval(self.update, 1 / FPS)
        self.events.append(event)

    def update(self, dt):
        if self.engine.figure is None:
            self.engine.new_figure()
        if self.engine.state == "start":
            self.engine.go_down(dt)
            self.draw_figures(self.ids.grid)
            self.draw_next_figure(self.ids.next_figure, dp(5), dp(5))
            TetrisApp.game.score = self.engine.score * 100
            if TetrisApp.game.score > TetrisApp.game.highscore:
                TetrisApp.game.highscore = TetrisApp.game.score

            if self.engine.state == "gameover":
                save(
                    "saves_highscore",
                    highscore=TetrisApp.game.highscore,
                )
                self.get_gameover_dialog()

    def draw_grid(self, wid, height, offset_y_axis, width, offset_x_axis):
        for i in range(height + 1):
            with wid.canvas:
                Color(1, 1, 1, 1)
                Line(
                    points=[
                        wid.x + offset_x_axis,
                        wid.y + self.tile * i + offset_y_axis * 2,
                        wid.x + wid.width - offset_x_axis,
                        wid.y + self.tile * i + offset_y_axis * 2,
                    ]
                )

        for j in range(width + 1):
            with wid.canvas:
                Line(
                    points=[
                        wid.x + self.tile * j + offset_x_axis * 2,
                        wid.y + offset_y_axis,
                        wid.x + self.tile * j + offset_x_axis * 2,
                        wid.y + wid.height - offset_y_axis,
                    ]
                )

    def draw_figures(self, wid):
        wid.canvas.clear()
        self.draw_grid(wid, self.engine.height, 0, self.engine.width, 0)
        if self.engine.figure is not None:
            for i in range(4):
                for j in range(4):
                    p = i * 4 + j
                    if p in self.engine.figure.image():
                        with wid.canvas:
                            Color(*COLORS[self.engine.figure.color])
                            Rectangle(
                                pos=(
                                    wid.x
                                    + self.engine.tile * (j + self.engine.figure.x)
                                    + 1,
                                    wid.y
                                    + wid.height
                                    - self.engine.tile
                                    - self.engine.tile * (i + self.engine.figure.y)
                                    + 1,
                                ),
                                size=(self.engine.tile - 2, self.engine.tile - 2),
                            )

        for i in range(self.engine.height):
            for j in range(self.engine.width):
                if self.engine.field[i][j] > 0:
                    with wid.canvas:
                        Color(*COLORS[self.engine.field[i][j]])
                        Rectangle(
                            pos=(
                                wid.x + self.engine.tile * j + 1,
                                wid.y
                                + wid.height
                                - self.engine.tile
                                - self.engine.tile * i
                                + 1,
                            ),
                            size=(self.engine.tile - 2, self.engine.tile - 2),
                        )

    def draw_next_figure(self, wid, offset_y_axis, offset_x_axis):
        wid.canvas.clear()
        self.draw_grid(wid, 4, offset_y_axis, 4, offset_x_axis)
        if self.engine.next_figure is not None:
            for i in range(4):
                for j in range(4):
                    p = i * 4 + j
                    if p in self.engine.next_figure.image():
                        with wid.canvas:
                            Color(*COLORS[self.engine.next_figure.color])
                            Rectangle(
                                pos=(
                                    wid.x
                                    + offset_x_axis * 2
                                    + self.engine.tile * j
                                    + 1,
                                    wid.y
                                    + wid.height
                                    - offset_y_axis * 2
                                    - self.engine.tile
                                    - self.engine.tile * i
                                    + 1,
                                ),
                                size=(self.engine.tile - 2, self.engine.tile - 2),
                            )

    def press_left_btn(self):
        self.engine.go_side(-1)
        TetrisApp.game.sound_click.play()

    def press_right_btn(self):
        self.engine.go_side(1)
        TetrisApp.game.sound_click.play()

    def press_rotate_btn(self):
        self.engine.rotate()
        TetrisApp.game.sound_click.play()

    def press_down_btn(self):
        self.engine.go_space()
        TetrisApp.game.sound_click.play()

    def press_pause_btn(self):
        for event in self.events:
            event.cancel()
        self.get_pause_dialog()

    def reset(self):
        self.engine.state = "restart"
        for event in self.events:
            event.cancel()
        self.ids.grid.canvas.clear()
        self.ids.next_figure.canvas.clear()
        self.engine.figure = None

    def get_gameover_dialog(self):
        if not self.gameover_dialog:
            self.gameover_dialog = MDDialog(
                auto_dismiss=False,
                radius=[
                    36,
                ],
                md_bg_color="#FFFFE0",
                type="custom",
                content_cls=GameOver(),
                buttons=[
                    MDFlatButton(
                        text="YES",
                        font_name="fonts/Komika_display.ttf",
                        on_release=lambda _: self.yes_btn_press(),
                    ),
                    MDFlatButton(
                        text="NO",
                        font_name="fonts/Komika_display.ttf",
                        on_release=lambda _: self.no_btn_press(),
                    ),
                ],
            )
        self.gameover_dialog.open()

    def yes_btn_press(self):
        self.gameover_dialog.dismiss()
        self.engine.__init__(H, W)

    def no_btn_press(self):
        self.reset()
        self.gameover_dialog.dismiss()
        TetrisApp.game.current = "welcome_screen"

    def get_pause_dialog(self):
        if not self.pause_dialog:
            self.pause_dialog = MyMDDialog(
                auto_dismiss=False,
                radius=[
                    36,
                ],
                md_bg_color="#FFFFE0",
                title="Pause",
                buttons=[
                    MDFlatButton(
                        text="Continue",
                        font_name="fonts/Komika_display.ttf",
                        on_release=lambda _: self.continue_btn_press(),
                    ),
                    MDFlatButton(
                        text="New Game",
                        font_name="fonts/Komika_display.ttf",
                        on_release=lambda _: self.newgame_btn_press(),
                    ),
                ],
            )
        self.pause_dialog.open()

    def newgame_btn_press(self):
        self.reset()
        self.pause_dialog.dismiss()
        self.engine.__init__(H, W)
        for event in self.events:
            event()

    def continue_btn_press(self):
        self.pause_dialog.dismiss()
        for event in self.events:
            event()


class GameOver(MDBoxLayout):
    pass


class TetrisApp(MDApp):
    version = StringProperty("")
    h = NumericProperty(0)
    w = NumericProperty(0)
    screen_divider = NumericProperty(0)

    def build(self):
        self.version = VERSION
        self.h = H
        self.w = W
        self.screen_divider = SCREEN_DIVIDER

        TetrisApp.game = Game(transition=FadeTransition())
        self.game.add_widget(LoadingScreen(name="loading_screen"))
        self.game.add_widget(WelcomeScreen(name="welcome_screen"))
        self.game.add_widget(GameScreen(name="game_screen"))
        self.game.add_widget(OptionsScreen(name="options_screen"))
        return self.game


if __name__ == "__main__":
    LabelBase.register(
        name="Rabbit",
        fn_regular="fonts/Komika_display.ttf",
        fn_bold="fonts/Komika_display_bold.ttf",
        fn_italic="fonts/KomikaTitle-Paint.ttf",
    )
    TetrisApp().run()
