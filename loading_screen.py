from kivy.uix.screenmanager import Screen
from kivy.clock import Clock


class LoadingScreen(Screen):
    def on_enter(self):
        Clock.schedule_once(self.game.load_music, 10)
        Clock.schedule_interval(self.loading_music_check, 0.1)

    def loading_music_check(self, dt):
        if len(self.game.music_list) == 4 and not self.game.music_loaded:
            self.game.music_mute_unmute()
            self.game.music.play()
            self.game.music_loaded = True
            self.game.current = "welcome_screen"
