![https://img.shields.io/badge/Python-3.9.7-blue](https://img.shields.io/badge/Python-3.9.7-blue) ![https://img.shields.io/badge/code%20style-black-000000.svg](https://img.shields.io/badge/code%20style-black-000000.svg)

# :yellow_heart: :blue_heart: Kivy Tetris :blue_heart: :yellow_heart:
Mobile game for Android in Python with Kivy framework and KivyMD library.

Watch gameplay on [YouTube](https://youtu.be/Z4CAB2a4ohg)

# Prerequisites
- [Kivy>=2.0.0](https://kivy.org/#home)
- [KivyMD>=0.104.2](https://kivymd.readthedocs.io/en/latest/)
