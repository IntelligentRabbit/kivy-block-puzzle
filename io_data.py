import json


def save(file_name, **kwargs):
    to_json = kwargs
    with open(f"{file_name}.json", "w") as f:
        json.dump(to_json, f)


def load(file_name):
    try:
        with open(f"{file_name}.json", "r") as f:
            from_json = json.load(f)
    except FileNotFoundError:
        from_json = {}
    return from_json
