from kivymd.uix.screen import MDScreen
from kivy.animation import Animation

from io_data import save


class OptionsScreen(MDScreen):
    def on_enter(self):
        self.animate_music_btn()
        self.animate_sound_btn()
        self.animate_change_bg_btn()

    def on_leave(self):
        self.ids.music_btn.opacity = 0
        self.ids.sound_btn.opacity = 0
        self.ids.change_bg_btn.opacity = 0

    def change_background(self):
        current_bg_number = self.game.backgrounds.index(self.game.background)
        bg_number = (current_bg_number + 1) % len(self.game.backgrounds)
        self.game.background = self.game.backgrounds[bg_number]
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
            background=self.game.background,
        )

    def sound_btn_press(self):
        self.game.sound_on = not self.game.sound_on
        self.game.sound_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
            background=self.game.background,
        )

    def music_btn_press(self):
        self.game.music_on = not self.game.music_on
        self.game.music_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
            background=self.game.background,
        )

    def return_btn_press(self):
        self.game.current = "welcome_screen"

    def animate_music_btn(self):
        anim = Animation(opacity=1, duration=1)
        anim.start(self.ids.music_btn)

    def animate_sound_btn(self):
        anim = Animation(opacity=1, duration=1)
        anim.start(self.ids.sound_btn)

    def animate_change_bg_btn(self):
        anim = Animation(opacity=1, duration=1)
        anim.start(self.ids.change_bg_btn)
